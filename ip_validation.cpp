#include <iostream>
#include <regex>

bool validateIPv4Prefix(const std::string& prefix) {
    std::regex pattern(R"((\d{1,3}\.){3}\d{1,3}/\d{1,2})");
    if (!std::regex_match(prefix, pattern)) {
        std::cout << "Bledny format prefiksu: " << prefix << std::endl;
        return false;
    }

    std::string addressString = prefix.substr(0, prefix.find("/"));
    std::string maskString = prefix.substr(prefix.find("/") + 1);
    int maskValue = std::stoi(maskString);

    if (maskValue <= 0 || maskValue > 30) {
        std::cout << "Nieprawidlowa dlugosc maski w prefiksie: " << prefix << std::endl;
        return false;
    }

    std::regex ipPattern(R"(\d{1,3})");
    std::smatch matches;
    std::vector<int> octets;
    while (std::regex_search(addressString, matches, ipPattern)) {
        int octet = std::stoi(matches.str());
        if (octet < 0 || octet > 255) {
            std::cout << "Nieprawidlowy adres IP w prefiksie: " << prefix << std::endl;
            return false;
        }
        octets.push_back(octet);
        addressString = matches.suffix();
    }

    if (octets.size() != 4) {
        std::cout << "Nieprawidlowy adres IP w prefiksie: " << prefix << std::endl;
        return false;
    }

    return true;
}