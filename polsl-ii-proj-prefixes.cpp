﻿#include <iostream>

extern bool validateIPv4Prefix(const std::string& prefix);
extern bool checkCommonAddresses(const std::string& prefix1, const std::string& prefix2);

int main(int argc, char* argv[]) {
    std::string prefix1 = "192.139.0.8/8";
    std::string prefix2 = "192.138.0.8/30";

    if (argc < 3 && argc != 1) {
        std::cout << "Bledna liczba argumentow!" << std::endl;
    }
    if (argc == 1) {
        std::cout << "Nie podano argumentow!" << std::endl;
        std::cout << "Program zostal uruchomiony z domyslnymi wartosciami: " + prefix1 + " " + prefix2 << std::endl;
    }
    else if (argc > 3) {
        std::cout << "Bledna liczba argumentow!" << std::endl;
        std::cout << "Podaj dwa argumenty w formacie prefixow IPv4 oddzielonych spacja: 192.168.0.0/24 192.168.0.8/30" << std::endl;
        return 1;
    }
    else {
        prefix1 = argv[1];
        prefix2 = argv[2];
    }

    if (!validateIPv4Prefix(prefix1) || !validateIPv4Prefix(prefix2)) {
        std::cout << "Bledny argument linii polecen!" << std::endl;
        std::cout << "Podaj argumenty w formacie prefixow IPv4 oddzielonych spacja: 192.168.0.0/24 192.168.0.8/30" << std::endl;
        return 1;
    }

    if (checkCommonAddresses(prefix1, prefix2)) {
        std::cout << "\n Wynik: \n tak" << std::endl;
    }
    else {
        std::cout << "\n Wynik: \n nie" << std::endl;
    }

    return 0;
}