#include <iostream>
#include <string>

unsigned int calculateMask(int mask) {
    unsigned int output = 0xFFFFFFFF;
    output <<= (32 - mask);
    return output;
}

unsigned int calculateAddress(const std::string& ipAddress) {
    unsigned int output = 0;
    unsigned int octet = 0;
    int shift = 24;

    for (size_t i = 0; i < ipAddress.length(); ++i) {
        if (ipAddress[i] == '.') {
            output |= (octet << shift);
            octet = 0;
            shift -= 8;
        }
        else {
            octet = (octet * 10) + (ipAddress[i] - '0');
        }
    }

    output |= (octet << shift);

    std::cout << "calculated int address: " << output << std::endl;

    return output;
}

void calculateMinAndMaxAddress(const std::string& prefix, unsigned int& minAddress, unsigned int& maxAddress) {
    std::string network = prefix.substr(0, prefix.find("/"));
    int mask = std::stoi(prefix.substr(prefix.find("/") + 1));

    unsigned int networkAddress = calculateAddress(network);
    unsigned int maskValue = calculateMask(mask);
    minAddress = (networkAddress)&maskValue;
    maxAddress = (networkAddress) | ~maskValue;
    std::cout << "network: " << network << std::endl;
    std::cout << "mask: " << mask << std::endl;
    std::cout << "min: " << minAddress << std::endl;
    std::cout << "max: " << maxAddress << std::endl;
}

bool checkCommonAddresses(const std::string& prefix1, const std::string& prefix2) {
    unsigned int minAddress1, maxAddress1, minAddress2, maxAddress2;

    calculateMinAndMaxAddress(prefix1, minAddress1, maxAddress1);
    calculateMinAndMaxAddress(prefix2, minAddress2, maxAddress2);

    if (minAddress1 <= minAddress2 && maxAddress2 <= maxAddress1) {
        return true;
    }
    if (minAddress2 <= minAddress1 && maxAddress1 <= maxAddress2) {
        return true;
    }

    return false;
}